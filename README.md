# Sunday Optimization Project

## Overview

After applying all optimizations, the batches of each frame are reduced from 16 to 9 when all objects are visible in the scene. Also, the number of steps need to render each frame of the scene was decreased from 19 to 12. And the size of the APK was brought down from 18 MB to 9 MB.

## Applied Optimizations

- **Added .gitignore file**

  As you mentioned before, there were a lot of irrelevant files that were committed and pushed wrongly on GIT. I've fixed the issue by adding a typical .gitignore file to the repo.

- **Refactored Codes and project structure**
  - Redesigned the Scripts folder structure:
    - Elements

      It is responsible for the scripts that are attached to the game elements like Enemy, Ball, and FinishTarget.

    - Models

      It includes the scripts that are held the data schema of objects. Currently, we only have the Level model.

    - Managers

      It contains manager scripts. Currently based on the simplicity of the project, we only have a Game Manager.

    - Utils

      It contains independent utilities that can be used without any coupling with other scripts. Currently, we have only an Analytics Manager.

  - Added Data folder to store each level data instead of using individual scenes for each level.

  - Used Singleton design pattern for the Game Manager and Analytics Manager and also Observer/Event-based design pattern for collision detection.

  - Cached multiple high-costed Unity API calls like Find(), GetComponent(), Camera.Main to reduce the cost of each frame process.

  - Put camera positioning logic in LateUpdate() event function instead of Update().

  - Refactored some names to be more readable and self-descriptive. For example, MyEventSystem has changed to AnalyticsManager.


- **Implemented dynamic level generation with Scriptable Objects**

    Based on designed levels, I've created a system to generate levels based on data assets that were stored in the Data folder as a Scriptable Object. Now for adding a new level, there is no need to set up a new scene. You can just add new data assets to the Data folder and add them to the game manager. For more details, please check the 'How to add levels' section.
    As I realized, Enemies and Finish Target are changed on each level. It is obvious if the dynamic factors change and levels are getting more complex, we need to revise the implemented system.

- **Fixed GameAnalytics SDK**

    I've found three issues with implementing GameAnalytics:

    - The compilation error of the missed assembly definition reference in your custom-defined assembly. As I realized, GameAnalytics automatically goes to the Default assembly (Assembly-Csharp). To fix the issue, we need to define a new .asmdef for GameAnalytics (in its root folder and also Editor folder) and just add its reference to our custom assembly definition. I believe that in small projects with short compilation time, making custom assemblies is not a very good idea and just increases the cost of maintenance. I've deleted the custom assembly, but I've used a singleton design pattern to keep its independence and logic encapsulation. At the same time, it is accessible across the project to log custom events.
    - Its android libraries dependencies are not resolved. I've resolved it once and pushed them to the project to prevent future resolutions.
    - I've just updated the SDK to the latest version.

- **Optimized Physic-Related Operations on each frame**

    - To reduce dependency of physic-related calculation form Frame rate, put the physic-related calculation to the FixedUpdate() event function.
    - Tweaked Fixed Time Step value to approach acceptable result between the accuracy of physics and its performance impact. By testing on more devices, I think it can be set to better value.

- **Optimized floor**

  - Marked it as a static game object to reduce lighting/shadowing costs.
  - Disabled its shadow casting, it just receives shadow now.
  - Replaced its Standard shader material to mobile-friendly material.
  - Modified the texture of its material to apply lighting effect without using the lights.
  - Modified compression settings of its texture.

- **Optimized Enemy and Ball**

  - Used a common simple material with enabled GPU instancing
  - Used Material Property Block to colorize the comon material
  - Marked the Enemy prefab as static game object

- **Used il2cpp to build for both 32-bit and 64-bit processors**

- **Optmized build settings**

  - Disabled 32-bit Display buffer.
  - Added Auto Graphic API to let the device choose better/compatible API (It increases the build size a little bit).
  - Used incremental Garbage Collection.
  - Enabled Prebake Collision Meshes.
  - Disabled Assert, Warning, and Logs. Just keep Exceptions and Errors enabled.

- **Reduced size of final APK from 18.3 MB to 9.8 MB**

  - Removed multiple scenes.
  - Reduced number of scripts and materials.
  - Compressed textures.
  - Splited APKs based on CPU architecture.

## Additional Notes

- As you mentioned not editing models and levels, I've tried to keep the visual aspect of the game as the same as the sent project. If it is possible to change the visual aspect, I think we can apply more optimization by baking light, using fake shadows (textures for static objects and blob projector for dynamic objects), using simpler shader materials, disabling shines and reflections, and ... .

- You mentioned an issue in lighting between editing mode and game/built mode. Honestly, I couldn't find any difference there. Maybe it is because the lighting toggle is off on edit mode.

## Download Optimized APks

You can download the built APKs for 32-bit and 64-bit processors from here:

[Download](https://drive.google.com/drive/folders/19SZg82JON2Mti4k-qMtuYiZuMBGml4B2?usp=sharing)

## How to add new levels

- Right click on Data folder and create new Level Scriptable Object.
- Fill the needed data (number of enemies, position of enemies, and position of finish target).
- Open Game Manager prefab and add new level to the list of levels.
