using UnityEngine;
using GameAnalyticsSDK;

public class AnalyticsManager : MonoBehaviour
{
    private static AnalyticsManager _instance;

    public static AnalyticsManager Instance
    {
        get
        {

            if (_instance == null)
            {
                _instance = FindObjectOfType<AnalyticsManager>();
                if (_instance == null)
                {
                    GameObject gameObject = new GameObject("AnalyticsManager");
                    _instance = gameObject.AddComponent<AnalyticsManager>();
                    DontDestroyOnLoad(gameObject);
                }
            }
            return _instance;
        }
    }
    
    private void LogProgressionEvent(GAProgressionStatus status, string parameter)
    {
        GameAnalytics.NewProgressionEvent(status, parameter);
    }

    public void Initialize()
    {
        GameAnalytics.Initialize();
    }

    public void LogLevelStartingEvent(int level)
    {
        LogProgressionEvent(GAProgressionStatus.Start, level.ToString());
    }
    
    public void LogLevelFailingEvent(int level)
    {
        LogProgressionEvent(GAProgressionStatus.Fail, level.ToString());
    }
    
    public void LogLevelCompletionEvent(int level)
    {
        LogProgressionEvent(GAProgressionStatus.Complete, level.ToString());
    }
}
