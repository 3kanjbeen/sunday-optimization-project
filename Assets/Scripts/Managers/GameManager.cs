﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private readonly string ballTag = "PlayerBall";
    private int _currentLevelID = 0;

    private LevelModel _currentLevelModel;
    private Vector3 _ballOriginialPosition;
    private List<GameObject> _enemies;

    [SerializeField] private List<LevelModel> _levelModels;
    [SerializeField] private GameObject _target;
    [SerializeField] private GameObject _enemy;
    [SerializeField] private GameObject _ball;

    private Rigidbody _ballRigidBody;

    private Camera _camera;

    void Awake()
    {
        AnalyticsManager.Instance.Initialize();
        _camera = Camera.main;
    }

    void Start()
    {
        InitGame();
    }

    private void InitGame()
    {
        _currentLevelID = 0;

        _enemies = new List<GameObject>();

        _ballOriginialPosition = _ball.transform.position;
        _ballRigidBody = _ball.GetComponent<Rigidbody>();

        FinishTarget.OnTrigger += FinishTarget_OnTrigger;

        GenerateLevel();
    }

    #region Level Generation
    private void GenerateLevel()
    {
        DestoryEnemies();

        GetLevelData();

        SetFinishTargetPosition();

        SpawnEnemies();

        Enemy.OnCollision += Enemy_OnCollision;

        AnalyticsManager.Instance.LogLevelStartingEvent(_currentLevelID);
    }

    private void ResetLevel()
    {
        AnalyticsManager.Instance.LogLevelFailingEvent(_currentLevelID);
        SetFinishTargetPosition();

        ResetBall();
    }

    private void GetLevelData()
    {
        if (_currentLevelID >= _levelModels.Count)
            _currentLevelID = 0;
        _currentLevelModel = _levelModels[_currentLevelID];
    }
    #endregion

    #region Collision Handlers
    private void FinishTarget_OnTrigger(Collider collider)
    {
        if (collider.gameObject.name != ballTag)
            return;

        AnalyticsManager.Instance.LogLevelCompletionEvent(_currentLevelID);
        _currentLevelID++;
        GenerateLevel();
        ResetBall();
    }

    private void Enemy_OnCollision(Collision collision)
    {
        if (collision.gameObject.name != ballTag)
            return;
        ResetLevel();
    }
    #endregion

    #region Elements-Related
    private void ResetBallRigidBody()
    {
        if (_ballRigidBody == null)
            return;

        _ballRigidBody.velocity = Vector3.zero;
        _ballRigidBody.angularVelocity = Vector3.zero;
    }

    private void ResetBall()
    {
        if (_ball == null)
            return;

        ResetBallRigidBody();

        _ball.transform.position = _ballOriginialPosition;
    }

    private void DestoryEnemies()
    {
        if (_enemies == null)
            return;

        foreach (GameObject e in _enemies)
            Destroy(e);

        _enemies.Clear();

        Enemy.OnCollision -= Enemy_OnCollision;
    }
    
    private void SpawnEnemies()
    {
        foreach (Vector3 enemyPostion in _currentLevelModel.EnemiesPositions)
        {
            _enemies.Add(Instantiate((GameObject)_enemy, enemyPostion, Quaternion.identity));
        }
    }

    private void SetFinishTargetPosition()
    {
        _target.transform.position = _currentLevelModel.TargetPosition;
    }
    #endregion
}
