using UnityEngine;

public class Ball : MonoBehaviour
{
    public float torqueAmount;
    private Vector3 cameraOffset;

    private Camera camera;
    private bool isPressing = false;
    private Vector3 originalPressPoint = Vector3.zero;

    private Vector3 diff;

    private Rigidbody ballRigidbody;

    private MaterialPropertyBlock propertyBlock;

    private void Awake()
    {
        camera = Camera.main;
    }

    private void Start()
    {
        SetMaterialColor();
        ballRigidbody = this.GetComponent<Rigidbody>();
        cameraOffset = this.transform.position - camera.transform.position;
    }

    private void SetMaterialColor()
    {
        if (propertyBlock == null)
            propertyBlock = new MaterialPropertyBlock();

        propertyBlock.SetColor("_Color", new Color(0, 0.021f, 1f, 1f));
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.SetPropertyBlock(propertyBlock);
    }

    private void LateUpdate()
    {
        camera.transform.position = ballRigidbody.position - cameraOffset;
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            if (!isPressing)
            {
                originalPressPoint = Input.mousePosition;
                isPressing = true;
            }
            else
            {
                diff = (originalPressPoint - Input.mousePosition).normalized;
                ballRigidbody.AddTorque((Vector3.forward * diff.x + Vector3.right * -diff.y) * torqueAmount, ForceMode.VelocityChange);
            }
        }
        else
        {
            isPressing = false;
        }
    }
}
