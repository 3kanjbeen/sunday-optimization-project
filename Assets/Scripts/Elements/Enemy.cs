using System;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static Action<Collision> OnCollision;

    MaterialPropertyBlock propertyBlock;
    private void Start()
    {
        SetMaterialColor();
    }

    private void SetMaterialColor()
    {
        if (propertyBlock == null)
            propertyBlock = new MaterialPropertyBlock();

        propertyBlock.SetColor("_Color", new Color(1f, 0.021f, 0, 1f));
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.SetPropertyBlock(propertyBlock);
    }

    private void OnCollisionEnter(Collision collision)
    {
        OnCollision?.Invoke(collision);
    }
}
