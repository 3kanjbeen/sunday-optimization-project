using System;
using UnityEngine;

public class FinishTarget : MonoBehaviour
{
    public static Action<Collider> OnTrigger;
    private void OnTriggerEnter(Collider collider)
    {
        OnTrigger?.Invoke(collider);
    }
}
