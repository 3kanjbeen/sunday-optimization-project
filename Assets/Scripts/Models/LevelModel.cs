﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( menuName = "Level Scriptable Object")]
public class LevelModel : ScriptableObject
{
    [SerializeField]  private int _id;
    [SerializeField]  private List<Vector3> _enemiesPositions;
    [SerializeField]  private Vector3 _targetPosition;

    public int ID { get => _id; set => _id = value; }
    public List<Vector3> EnemiesPositions { get => _enemiesPositions; set => _enemiesPositions = value; }

    public Vector3 TargetPosition { get => _targetPosition; set => _targetPosition = value; }

}
